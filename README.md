# README

## Client

A create-react-app containing a simple form. Created using Formik and yup for validation.

To run the client, navigate to /client/web and run npm start.

## Server

Dotnet 6 web api, with connection available to SqlServer.

### Setup

Requires docker.

Start a sqlserver container with the following parameters.
<pre>
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=Password123!" -e "MSSQL_PID=Express" -p 1433:1433 -d mcr.microsoft.com/mssql/server:2019-latest 
</pre>