﻿using GreenwoodCampbell.DAL.Interfaces;
using GreenwoodCampbell.Logic.Services;
using Optional;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreenwoodCampbell.Logic.Managers;

/// <summary>
/// Interface representing <c>FormManager</c>
/// </summary>
public interface IFormManager
{
    /// <summary>
    /// Create the form and save it to the database.
    /// </summary>
    /// <param name="form">The form.</param>
    /// <returns></returns>
    Task<Option<Form, string>> CreateFormAsync(Form form);

    /// <summary>
    /// Send the form via email to the configured email address.
    /// </summary>
    /// <param name="form">The form.</param>
    /// <param name="recpient">The recipient email.</param>
    /// <returns></returns>
    Task<Option<Form, string>> SendEmailAsync(Form? form, string? recpient);

    /// <summary>
    /// Get a random form entry from the database.
    /// </summary>
    /// <returns></returns>
    Task<Option<Form, string>> GetRandomFormAsync();
}

/// <summary>
/// Data manager for handling the <c>Form</c> data type.
/// </summary>
internal class FormManager : IFormManager
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMailService _mailService;

    public FormManager(IUnitOfWork unitOfWork, IMailService mailService)
    {
        _unitOfWork = unitOfWork;
        _mailService = mailService;
    }

    /// <summary>
    /// Create the form and save it to the database.
    /// </summary>
    /// <param name="form">The form.</param>
    /// <returns></returns>
    public async Task<Option<Form, string>> CreateFormAsync(Form form)
    {
        try
        {
            if (form == null)
            {
                return Option.None<Form, string>($"Failed to create form: Invalid form.");
            }

            Form result = await _unitOfWork.Forms.CreateAsync(form);

            await _unitOfWork.CompleteAsync();

            return Option.Some<Form, string>(result);
        }
        catch (Exception ex)
        {
            return Option.None<Form, string>($"Failed to create form: {ex.Message}");
        }
    }

    /// <summary>
    /// Send the form via email to the configured email address.
    /// </summary>
    /// <param name="form">The form.</param>
    /// <param name="recpient">The recipient email.</param>
    /// <returns></returns>
    public async Task<Option<Form, string>> SendEmailAsync(Form? form, string? recipient)
    {
        try
        {
            if (form == null)
            {
                return Option.None<Form, string>($"Failed to send email: Invalid form.");
            }

            Option<bool, string> result = await _mailService.SendEmail(recipient, "Greenwood Campbell Tech Test", form);

            return result.Map(_ => form);
        }
        catch (Exception ex)
        {
            return Option.None<Form, string>($"Failed to send email: {ex.Message}");
        }
    }

    /// <summary>
    /// Get a random form entry from the database.
    /// </summary>
    /// <returns></returns>
    public async Task<Option<Form, string>> GetRandomFormAsync()
    {
        try
        {
            Form? result = await _unitOfWork.Forms.GetRandomAsync();

            if (result == null)
            {
                return Option.None<Form, string>($"Failed to get form: No forms available.");
            }

            return Option.Some<Form, string>(result);
        }
        catch (Exception ex)
        {
            return Option.None<Form, string>($"Failed to get form: {ex.Message}");
        }
    }
}
