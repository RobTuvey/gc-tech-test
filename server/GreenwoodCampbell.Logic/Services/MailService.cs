﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Optional;
using System.Net;
using System.Net.Mail;

namespace GreenwoodCampbell.Logic.Services;

/// <summary>
/// Interface representing the <c>MailService</c>
/// </summary>
public interface IMailService
{
    /// <summary>
    /// Send an email to the specified recipient, using the subject and body.
    /// </summary>
    /// <param name="recipient">The recipient email.</param>
    /// <param name="subject">The email subject.</param>
    /// <param name="body">The email body.</param>
    /// <returns></returns>
    Task<Option<bool, string>> SendEmail(string recipient, string subject, object body);
}

/// <summary>
/// Service for sending emails.
/// </summary>
internal class MailService : IMailService
{
    private readonly IConfiguration _configuration;

    public MailService(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    /// <summary>
    /// Send an email to the specified recipient, using the subject and body.
    /// </summary>
    /// <param name="recipient">The recipient email.</param>
    /// <param name="subject">The email subject.</param>
    /// <param name="body">The email body.</param>
    /// <returns></returns>
    public async Task<Option<bool, string>> SendEmail(string recipient, string subject, object body)
    {
        var from = new MailAddress(_configuration["GreenwoodCampbell:Email"], _configuration["GreenwoodCampbell:Sender"]);
        var to = new MailAddress(recipient);

        // Strip out curly braces.
        string bodyText = string.Empty;

        if (body != null)
        {
            bodyText = JsonConvert.SerializeObject(body);
            bodyText = bodyText[1..^1];
        }

        var smptClient = new SmtpClient
        {
            Host = "smtp.gmail.com",
            Port = 587,
            EnableSsl = true,
            DeliveryMethod = SmtpDeliveryMethod.Network,
            UseDefaultCredentials = false,            
            Credentials = new NetworkCredential(from.Address, _configuration["GreenwoodCampbell:EmailPassword"])
        };

        try
        {
            using (var message = new MailMessage(from, to) { Subject = subject, Body = bodyText })
            {
                await smptClient.SendMailAsync(message);
            }

            return Option.Some<bool, string>(true);
        }
        catch (Exception ex)
        {
            return Option.None<bool, string>(ex.Message);
        }
    }
}
