﻿using Autofac;
using GreenwoodCampbell.Logic.Managers;
using GreenwoodCampbell.Logic.Services;

namespace GreenwoodCampbell.Logic;

/// <summary>
/// Autofac module to register dependencies.
/// </summary>
public class ContainerModule : Autofac.Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder
            .RegisterType<FormManager>()
            .As<IFormManager>()
            .InstancePerDependency();

        builder
            .RegisterType<MailService>()
            .As<IMailService>()
            .InstancePerDependency();
    }
}
