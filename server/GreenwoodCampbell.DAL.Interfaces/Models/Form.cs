﻿using System.ComponentModel.DataAnnotations;

namespace GreenwoodCampbell.DAL.Interfaces;

public class Form
{
    public Guid Id { get; set; } = Guid.NewGuid();

    [Required]
    public string FirstName { get; set; } = string.Empty;

    [Required]
    public string LastName { get; set; } = string.Empty;

    [Required]
    [EmailAddress]
    public string Email { get; set; } = string.Empty;

    [Required]
    [Phone]
    public string PhoneNumber { get; set; } = string.Empty;

    [Required]
    [Range(18, 99)]
    public int Age { get; set; }

    public string Nationality { get; set; } = string.Empty;
}
