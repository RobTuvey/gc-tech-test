﻿
namespace GreenwoodCampbell.DAL.Interfaces;

/// <summary>
/// Basic representation for a unit of work.
/// </summary>
public interface IUnitOfWork : IDisposable
{
    /// <summary>
    /// The form repository.
    /// </summary>
    IRepository<Form> Forms { get; }

    /// <summary>
    /// Complete and save changes.
    /// </summary>
    /// <returns></returns>
    Task<int> CompleteAsync();
}