﻿namespace GreenwoodCampbell.DAL.Interfaces;

/// <summary>
/// Basic repository interface.
/// </summary>
/// <typeparam name="TModel">The model type.</typeparam>
public interface IRepository<TModel>
{
    
    /// <summary>
    /// Create a new instance of a model.
    /// </summary>
    /// <param name="model">The new model.</param>
    /// <returns></returns>
    Task<TModel> CreateAsync(TModel model);
    Task DeleteAsync(Guid id);

    /// <summary>
    /// Get random entry from the table.
    /// </summary>
    /// <returns></returns>
    Task<TModel?> GetRandomAsync();
}
