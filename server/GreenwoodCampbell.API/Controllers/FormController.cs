﻿using GreenwoodCampbell.API.Models;
using GreenwoodCampbell.DAL.Interfaces;
using GreenwoodCampbell.Logic.Managers;
using Microsoft.AspNetCore.Mvc;
using Optional;

namespace GreenwoodCampbell.API.Controllers;

[ApiController]
[Route("form")]
public class FormController : ControllerBase
{
    private readonly IFormManager _formManager;

    public FormController(IFormManager formManager)
    {
        _formManager = formManager;
    }

    /// <summary>
    /// Create and save a new form.
    /// </summary>
    /// <param name="form"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> Create([FromBody] Form form)
    {
        Option<Form, string> result = await _formManager.CreateFormAsync(form);

        return result.Match<IActionResult>(
            form => Ok(form),
            error => BadRequest(error)
        );
    }

    /// <summary>
    /// Send the form via email.
    /// </summary>
    /// <param name="model">The input model containing the form and the recipient.</param>
    /// <returns></returns>
    [Route("email"), HttpPost]
    public async Task<IActionResult> SendEmail([FromBody] SendEmailModel model)
    {
        Option<Form, string> result = await _formManager.SendEmailAsync(model?.Form, model?.Email);

        return result.Match<IActionResult>(
            form => Ok(form),
            error => BadRequest(error)
        );
    }

    /// <summary>
    /// Get a random form from the database.
    /// </summary>
    /// <returns></returns>
    [Route("random"), HttpGet()]
    public async Task<IActionResult> GetRandomForm()
    {
        Option<Form, string> result = await _formManager.GetRandomFormAsync();

        return result.Match<IActionResult>(
            form => Ok(form),
            error => BadRequest(error)
        );
    }
}