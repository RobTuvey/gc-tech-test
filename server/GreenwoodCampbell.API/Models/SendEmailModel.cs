﻿using GreenwoodCampbell.DAL.Interfaces;

namespace GreenwoodCampbell.API.Models;

/// <summary>
/// Input model for sending a form via email.
/// </summary>
public class SendEmailModel
{
    /// <summary>
    /// The form to send.
    /// </summary>
    public Form? Form { get; set; }

    /// <summary>
    /// The recipient email.
    /// </summary>
    public string Email { get; set; } = string.Empty;
}
