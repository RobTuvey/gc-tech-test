﻿using GreenwoodCampbell.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace GreenwoodCampbell.DAL;

/// <summary>
/// Basic repository.
/// </summary>
/// <typeparam name="TModel">The type to target.</typeparam>
internal class Repository<TModel> : IRepository<TModel> where TModel : class
{
    private readonly DbContext _context;
    private readonly DbSet<TModel> _models;

    public Repository(DbContext context)
    {
        _context = context;
        _models = context.Set<TModel>();
    }

    public async Task<TModel> CreateAsync(TModel model)
    {
        var result = await _models.AddAsync(model);

        return result.Entity;
    }

    public async Task<TModel?> GetRandomAsync()
    {
        int count = await _models.CountAsync();
        Random random = new Random();
        int toSkip = random.Next(0, count);

        return _models.Skip(toSkip).FirstOrDefault();
    }
}
