﻿using GreenwoodCampbell.DAL.Interfaces;

namespace GreenwoodCampbell.DAL;

/// <summary>
/// Basic representation of a unit of work.
/// </summary>
public class UnitOfWork : IUnitOfWork
{
    /// <summary>
    /// The forms repository.
    /// </summary>
    public IRepository<Form> Forms { get; }

    private readonly ApplicationContext _context;

    public UnitOfWork(ApplicationContext context)
    {
        _context = context;

        Forms = new Repository<Form>(_context);
    }

    /// <summary>
    /// Apply all changes to the database.
    /// </summary>
    /// <returns></returns>
    private int Complete()
    {
        return _context.SaveChanges();
    }

    /// <summary>
    /// Apply all changes to the database in an asynchronous operation.
    /// </summary>
    /// <returns></returns>
    public async Task<int> CompleteAsync()
    {
        return await _context.SaveChangesAsync();
    }

    public void Dispose()
    {
        Complete();
    }
}
