﻿using GreenwoodCampbell.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace GreenwoodCampbell.DAL;

/// <summary>
/// The database context representing the application.
/// </summary>
public class ApplicationContext : DbContext
{
    /// <summary>
    /// The form database table.
    /// </summary>
    public DbSet<Form> Forms { get; }

    public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
    {
        Forms = Set<Form>();
    }
}
