﻿using Autofac;
using GreenwoodCampbell.DAL.Interfaces;

namespace GreenwoodCampbell.DAL;

/// <summary>
/// Autofac module to register dependencies.
/// </summary>
public class ContainerModule : Autofac.Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder
            .RegisterGeneric(typeof(Repository<>))
            .AsImplementedInterfaces()
            .InstancePerDependency();

        builder
            .RegisterType<UnitOfWork>()
            .As<IUnitOfWork>()
            .InstancePerDependency();
    }
}