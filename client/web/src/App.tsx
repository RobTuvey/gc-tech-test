import './App.css';
import { Form } from "./Form"
import { EmailForm } from "./EmailForm"
import { FormModel } from './Form/FormModel';
import { useState } from 'react';
import axios from "axios";

import { Modal, Box } from "@mui/material";

function App() {
  const [ form, setForm ] = useState<FormModel>();
  const [ showModal, setShowModal ] = useState(false);

  const onSubmit = (form: FormModel) => {
    setForm(form);
    axios.post("/form", form);
  }

  const onEmailSubmit = (form: FormModel) => {
    setForm(form);
    setShowModal(true);
  }

  const onFetchForm = () => {
    axios.get<FormModel>("/form/random")
      .then(response => setForm(response.data))
  }

  const sendEmail = (email: string) => {
    axios.post("/form/email", { form, email});
  }

  return (
    <div className="App">
      <Form
        form={form}
        onSubmit={onSubmit}
        onEmailSubmit={onEmailSubmit}
        onFetchForm={onFetchForm}
      />
      <Modal
        open={showModal}
      >
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 400,
            bgcolor: 'background.paper',
            border: '2px solid #000',
            boxShadow: 24,
            p: 4,
          }}
        >
          <EmailForm
            onSubmit={sendEmail}
            onCancel={() => setShowModal(false)}
          />
        </Box>
      </Modal>
    </div>
  );
}

export default App;
