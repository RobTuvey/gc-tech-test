import { useFormik } from "formik";
import { Box, Button, TextField } from "@mui/material";
import { object, string } from "yup";

export interface IEmailFormProps {
    onSubmit: (email: string) => void,
    onCancel: () => void
}

export function EmailForm({
    onSubmit,
    onCancel
}: IEmailFormProps) {
    const validateForm = async () => {
        const errors = await formik.validateForm();
        formik.setErrors(errors);

        const values = Object.values(errors);
        return values.length == 0;
    }

    const submit = async () => {
        const valid = await validateForm();
        if (valid) {
            onSubmit(formik.values.email);
        }
    };

    const formik = useFormik({
        initialValues: { email: '' },
        onSubmit: submit,
        validationSchema: object({
            email: string().required("Email is required.").email("Invalid email.")
        })
    });

    return (
        <form 
            onSubmit={formik.handleSubmit}
            style={{
                display: 'flex',
                flexDirection: 'column'
            }}
        >
            <TextField
                {...formik.getFieldProps("email")}
                label="Email"
                color="success"
                InputProps={{
                    className: "input"
                }}
            />
            <div style={{ marginBottom: "10px" }}>
                {formik.errors["email"]}
            </div>

            <Box
                sx={{
                    display: "flex",
                    justifyContent: "space-between"
                }}
            >
                <Button
                    variant="contained"
                    color="error"
                    onClick={onCancel}
                >
                    Cancel
                </Button>

                <Button
                    variant="contained"
                    color="success"
                    sx={{marginRight: "10px"}}
                    onClick={submit}
                >
                    Send
                </Button>
            </Box>
        </form>
    );
}