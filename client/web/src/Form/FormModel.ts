import * as yup from "yup";

export const FormValidation = yup.object({
    firstName: yup.string().required("First name is required.").default(""),
    lastName: yup.string().required("Last name is required.").default(""),
    email: yup.string().required("Email is required.").email("Invalid email.").default(""),
    phoneNumber: yup.string().required("Phone number is required.").matches(/^[0-9]+$/g),
    age: yup.number().min(18, "Minimum age is 18.").max(99, "Maximum age is 99.").required("Age is required."),
    nationality: yup.string().default("")
})

export interface FormModel {
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    age: number;
    nationality?: string;
}