import "./css.css"
import { useFormik } from "formik";
import { Box, Button, TextField } from "@mui/material";

import { FormModel, FormValidation } from "./FormModel";
import { useEffect } from "react";



export interface IFormProps {
    form?: FormModel,
    onSubmit: (form: FormModel) => void,
    onEmailSubmit: (form: FormModel) => void,
    onFetchForm: () => void
}

const initialForm = {
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    age: 0,
    nationality: ""
};

export function Form({
    form,
    onSubmit,
    onEmailSubmit,
    onFetchForm
}: IFormProps) {
    const formik = useFormik({
        initialValues: form || initialForm,
        onSubmit: onSubmit,
        validationSchema: FormValidation
    });

    useEffect(() => {
        if (form) {
            formik.setValues(values => ({...form}), false);
        }
        else {
            formik.setValues(values => ({...initialForm}), false);
        }
    }, [form]);

    const validateForm = async () => {
        formik.setTouched({
            firstName: true,
            lastName: true,
            email: true,
            phoneNumber: true,
            age: true
        });
        const errors = await formik.validateForm();
        formik.setErrors(errors);

        const values = Object.values(errors);
        return values.length == 0;
    }

    const submit = async () => {
        const valid = await validateForm();
        if (valid) {
            onSubmit(formik.values);
        }
    };

    const submitEmail = async () => {
        const valid = await validateForm();
        if (valid) {
            onEmailSubmit(formik.values);
        }
    };

    const clearForm = () => {
        formik.setTouched({
            firstName: false,
            lastName: false,
            email: false,
            phoneNumber: false,
            age: false
        });
        formik.setValues(values => ({...initialForm}), false);
    }

    return (
        <form
            className="form"
            onSubmit={formik.handleSubmit}
        >
            <TextField
                {...formik.getFieldProps("firstName")}
                label="First Name"
                color="success"
                InputProps={{
                    className: "input"
                }}
            />
            <div style={{ marginBottom: "15px"}}>
                {formik.touched["firstName"] && formik.errors["firstName"]}
            </div>

            <TextField
                {...formik.getFieldProps("lastName")}
                label="Last Name"
                color="success"
                InputProps={{
                    className: "input"
                }}
            />
            <div style={{ marginBottom: "15px"}}>
                {formik.touched["lastName"] && formik.errors["lastName"]}
            </div>

            <TextField
                {...formik.getFieldProps("email")}
                label="Email"
                color="success"
                InputProps={{
                    className: "input"
                }}
            />
            <div style={{ marginBottom: "15px"}}>
                {formik.touched["email"] && formik.errors["email"]}
            </div>

            <TextField
                {...formik.getFieldProps("phoneNumber")}
                label="Phone Number"
                color="success"
                InputProps={{
                    className: "input"
                }}
            />
            <div style={{ marginBottom: "15px"}}>
                {formik.touched["phoneNumber"] && formik.errors["phoneNumber"]}
            </div>

            <TextField
                {...formik.getFieldProps("age")}
                label="Age"color="success"
                InputProps={{
                    className: "input"
                }}
            />
            <div style={{ marginBottom: "15px"}}>
                {formik.touched["age"] && formik.errors["age"]}
            </div>

            <TextField
                {...formik.getFieldProps("nationality")}
                label="Nationality"color="success"
                sx={{
                    marginBottom: "15px"
                }}
                InputProps={{
                    className: "input"
                }}
            />

            <Box
                sx={{
                    display: "flex"
                }}
            >
                <Button
                    variant="contained"
                    color="primary"
                    sx={{marginRight: "10px"}}
                    onClick={submit}
                >
                    Save
                </Button>

                <Button
                    variant="contained"
                    color="primary"
                    sx={{marginRight: "10px"}}
                    onClick={submitEmail}
                >
                    Email
                </Button>

                <Button
                    variant="contained"
                    color="success"
                    sx={{marginRight: "10px"}}
                    onClick={onFetchForm}
                >
                    Get
                </Button>

                <Button
                    variant="contained"
                    color="error"
                    onClick={clearForm}
                >
                    Clear
                </Button>
            </Box>
        </form>
    );
}